import CompressionPlugin from 'compression-webpack-plugin';
import CopyPlugin from 'copy-webpack-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { resolve } from 'path';
import { WebpackPluginInstance } from 'webpack';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

import { DIST, PUBLIC } from '../common/constants';

const plugins: WebpackPluginInstance[] = [
  new CompressionPlugin({
    filename: '[path][base].br',
    algorithm: 'brotliCompress',
    test: /\.(js|css|html|svg)$/,
    compressionOptions: {
      level: 11,
    },
    threshold: 10240,
    minRatio: 0.8,
    deleteOriginalAssets: false,
  }),
  new CompressionPlugin({
    filename: '[path][base].gz',
    algorithm: 'gzip',
    test: /\.(js|css|html|svg)$/,
    compressionOptions: {
      level: 9,
    },
    threshold: 10240,
    minRatio: 0.8,
    deleteOriginalAssets: false,
  }),
  new CopyPlugin({
    patterns: [
      {
        from: resolve(PUBLIC, '.htaccess'),
        to: `${DIST}`,
      },
    ],
    options: {
      concurrency: 100,
    },
  }),

  new BundleAnalyzerPlugin({
    generateStatsFile: true,
    statsOptions: { source: false },
    analyzerMode: 'static',
  }),
  new MiniCssExtractPlugin({
    linkType: 'text/css',
    filename: '[name].[contenthash].css',
    chunkFilename: '[id].[contenthash].css',
    experimentalUseImportModule: true,
  }),
  new ForkTsCheckerWebpackPlugin(),
];

export { plugins };
