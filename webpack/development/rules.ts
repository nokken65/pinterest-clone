import { resolve } from 'path';
import { RuleSetRule } from 'webpack';

import { SRC } from '../common/constants';

const typescriptRule: RuleSetRule = {
  test: /\.tsx?$/,
  loader: 'esbuild-loader',
  options: { loader: 'tsx', target: 'es2015' },
  exclude: /node_modules/,
};

const cssRule: RuleSetRule = {
  test: /\.css$/i,
  use: [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1,
      },
    },
    'postcss-loader',
  ],
  exclude: /\.module\.css$/,
};

const sassRule: RuleSetRule = {
  test: /\.s[ac]ss$/i,
  use: [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        importLoaders: 2,
      },
    },
    'postcss-loader',
    {
      loader: 'sass-loader',
      options: {
        sourceMap: true,
      },
    },
    {
      loader: 'sass-resources-loader',
      options: {
        resources: [
          resolve(SRC, 'shared/styles/colors.scss'),
          resolve(SRC, 'shared/styles/variables.scss'),
          resolve(SRC, 'shared/styles/mixins.scss'),
        ],
      },
    },
  ],
  exclude: /\.module\.scss$/,
};

const sassModuleRule: RuleSetRule = {
  test: /\.s[ac]ss$/i,
  use: [
    'style-loader',
    'css-modules-typescript-loader',
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1,
        modules: {
          localIdentName: '[local]-[hash:base64:10]',
        },
      },
    },
    'postcss-loader',
    {
      loader: 'sass-loader',
      options: {
        sourceMap: true,
      },
    },
    {
      loader: 'sass-resources-loader',
      options: {
        resources: [
          resolve(SRC, 'shared/styles/colors.scss'),
          resolve(SRC, 'shared/styles/variables.scss'),
          resolve(SRC, 'shared/styles/mixins.scss'),
        ],
      },
    },
  ],
  include: /\.module\.scss$/,
};

export { cssRule, sassModuleRule, sassRule, typescriptRule };
