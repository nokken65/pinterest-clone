import { resolve } from 'path';

import { PUBLIC } from '../common/constants';

const devServer = {
  hot: true,
  port: 3000,
  open: false,
  historyApiFallback: true,
  compress: true,
  static: {
    directory: resolve(PUBLIC),
    publicPath: '/',
  },
};

export { devServer };
