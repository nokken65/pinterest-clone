import { reflect } from '@effector/reflect';

import { Form } from '@/shared/components';

import { registerForm } from '../../forms';

export const AgeField = reflect({
  view: Form.Field,
  bind: {
    type: 'number',
    name: 'age',
    placeholder: 'Age',
    value: registerForm.fields.age.$value,
    onChange: registerForm.fields.age.onChange,
    isDirty: registerForm.fields.age.$isDirty,
    isValid: registerForm.fields.age.$isValid,
    errors: registerForm.fields.age.$errors,
  },
});
