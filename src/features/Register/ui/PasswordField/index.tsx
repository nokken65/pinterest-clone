import { reflect } from '@effector/reflect';

import { Form } from '@/shared/components';

import { registerForm } from '../../forms';

export const PasswordField = reflect({
  view: Form.PasswordField,
  bind: {
    type: 'password',
    name: 'password',
    placeholder: 'Password',
    value: registerForm.fields.password.$value,
    onChange: registerForm.fields.password.onChange,
    isDirty: registerForm.fields.password.$isDirty,
    isValid: registerForm.fields.password.$isValid,
    errors: registerForm.fields.password.$errors,
  },
});
