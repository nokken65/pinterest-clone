import { reflect } from '@effector/reflect';
import { Store } from 'effector';
import { useNavigate } from 'react-router-dom';

import { Button } from '@/shared/components';

import { events, selectors } from '../../model';
import { Step } from '../../model/model';
import styles from './index.module.scss';

type NextStepButtonViewProps = {
  steps: Step[];
  currentStep: number;
  onNextStep: () => void;
  isDirty: boolean;
  isValid: boolean;
};

const NextStepButtonView = ({
  steps,
  currentStep,
  onNextStep,
  isDirty,
  isValid,
}: NextStepButtonViewProps) => {
  const navigate = useNavigate();

  const goNext = () => {
    onNextStep();
    navigate(`/signup/${steps[currentStep].path}`);
  };

  return (
    <Button
      className={styles.btn}
      disabled={!isDirty || !isValid}
      label='Next'
      onClick={goNext}
    />
  );
};

type NextButtonProps = {
  isDirty: Store<boolean>;
  isValid: Store<boolean>;
};

export const NextStepButton = ({
  isDirty,
  isValid,
  ...props
}: NextButtonProps) => {
  return (
    <>
      {reflect({
        view: NextStepButtonView,
        bind: {
          steps: selectors.$steps,
          currentStep: selectors.$currentStep,
          onNextStep: events.onNextStep,
          isDirty,
          isValid,
        },
      })(props)}
    </>
  );
};
