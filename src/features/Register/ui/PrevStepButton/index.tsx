import { reflect } from '@effector/reflect';
import PrevIcon from '@icons/arrow.svg';
import { useNavigate } from 'react-router-dom';

import { Button } from '@/shared/components';

import { registerForm } from '../../forms';
import { events, selectors } from '../../model';
import styles from './index.module.scss';

type PrevStepButtonViewProps = {
  currentStep: number;
  onPrevStep: () => void;
  resetRegisterForm: () => void;
};

const PrevStepButtonView = ({
  currentStep,
  onPrevStep,
  resetRegisterForm,
}: PrevStepButtonViewProps) => {
  const navigate = useNavigate();

  const goBack = () => {
    if (currentStep === 1) {
      resetRegisterForm();
      navigate('/');
    } else {
      onPrevStep();
      navigate(-1);
    }
  };

  return <Button className={styles.btn} icon={<PrevIcon />} onClick={goBack} />;
};

export const PrevStepButton = reflect({
  view: PrevStepButtonView,
  bind: {
    currentStep: selectors.$currentStep,
    onPrevStep: events.onPrevStep,
    resetRegisterForm: registerForm.reset,
  },
});
