// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'btn': string;
  'btn--valid': string;
  'container': string;
  'name--invalid': string;
  'wrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
