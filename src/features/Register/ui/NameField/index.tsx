import { reflect } from '@effector/reflect';
import EditIcon from '@icons/edit.svg';
import SuccessIcon from '@icons/success.svg';
import clsx from 'clsx';
import { useState } from 'react';

import { Button, FieldProps, Form, Heading } from '@/shared/components';

import { registerForm } from '../../forms';
import styles from './index.module.scss';

type NameFieldViewProps = FieldProps<string>;

const NameFieldView = ({ ...props }: NameFieldViewProps) => {
  const [editMode, setEditMode] = useState<boolean>(false);

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <Heading
          className={clsx(
            (!props.isValid || !props.isDirty) && styles['name--invalid'],
          )}
          size='2xl'
          title={props.value ? props.value : '____'}
        />
        <Button
          className={clsx(
            styles.btn,
            props.isValid && props.isDirty && styles['btn--valid'],
          )}
          icon={
            !props.isValid || !props.isDirty ? <EditIcon /> : <SuccessIcon />
          }
          onClick={() => setEditMode(!editMode)}
        />
      </div>
      {editMode && <Form.Field {...props} />}
      {!editMode && (!props.isValid || !props.isDirty) && (
        <Form.ErrorsFeedback errors={props.errors} />
      )}
    </div>
  );
};

export const NameField = reflect({
  view: NameFieldView,
  bind: {
    type: 'text',
    name: 'name',
    placeholder: 'Name',
    value: registerForm.fields.name.$value,
    onChange: registerForm.fields.name.onChange,
    isDirty: registerForm.fields.name.$isDirty,
    isValid: registerForm.fields.name.$isValid,
    errors: registerForm.fields.name.$errors,
  },
});
