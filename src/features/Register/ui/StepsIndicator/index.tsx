/* eslint-disable react/no-array-index-key */
import { reflect } from '@effector/reflect';
import clsx from 'clsx';

import { selectors } from '../../model';
import styles from './index.module.scss';

type StepsIndicatorProps = {
  count: number;
  current: number;
};

const StepsIndicatorView = ({ current, count }: StepsIndicatorProps) => {
  return (
    <div className={styles.wrapper}>
      {[...Array(count).keys()].map((_, i) => (
        <span
          className={clsx(styles.dot, i + 1 <= current && styles.completed)}
          key={i}
        />
      ))}
    </div>
  );
};

export const StepsIndicator = reflect({
  view: StepsIndicatorView,
  bind: {
    count: selectors.$stepCount,
    current: selectors.$currentStep,
  },
});
