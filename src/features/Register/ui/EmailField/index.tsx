import { reflect } from '@effector/reflect';

import { Form } from '@/shared/components';

import { registerForm } from '../../forms';

export const EmailField = reflect({
  view: Form.Field,
  bind: {
    type: 'email',
    name: 'email',
    placeholder: 'Email address',
    value: registerForm.fields.email.$value,
    onChange: registerForm.fields.email.onChange,
    isDirty: registerForm.fields.email.$isDirty,
    isValid: registerForm.fields.email.$isValid,
    errors: registerForm.fields.email.$errors,
  },
});
