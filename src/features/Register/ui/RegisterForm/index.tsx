import { PropsWithChildren } from 'react';

import { Form } from '@/shared/components';

type RegisterFormProps = PropsWithChildren<{}>;

export const RegisterForm = ({ children }: RegisterFormProps) => {
  return <Form>{children}</Form>;
};
