import { createEvent, createStore } from 'effector';

import { Step } from './model';

const $steps = createStore<Step[]>([
  {
    path: 'step1',
  },
  {
    path: 'step2',
  },
  {
    path: 'step3',
  },
]);

const $stepCount = $steps.map((steps) => steps.length);

const onNextStep = createEvent();
const onPrevStep = createEvent();

const $currentStep = createStore<number>(1)
  .on(onNextStep, (state) =>
    state + 1 >= $stepCount.getState() ? state : state + 1,
  )
  .on(onPrevStep, (state) => (state - 1 <= 1 ? 1 : state - 1));

export const events = {
  onNextStep,
  onPrevStep,
};

export const selectors = {
  $currentStep,
  $stepCount,
  $steps,
};
