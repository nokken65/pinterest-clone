import { sample } from 'effector';
import { createForm } from 'effector-forms';
import * as yup from 'yup';

import { createRule } from '@/shared/utils';

export const registerForm = createForm({
  fields: {
    email: {
      init: '',
      rules: [
        createRule<string>({
          name: 'email',
          schema: yup
            .string()
            .required("You missed a spot! Don't forget to add your email.")
            .matches(
              /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              { excludeEmptyString: true, message: 'Not a valid email.' },
            ),
        }),
      ],
      validateOn: ['change'],
    },
    password: {
      init: '',
      rules: [
        createRule<string>({
          name: 'password',
          schema: yup
            .string()
            .required("You missed a spot! Don't forget to add your password.")
            .min(8, 'Should be 8 chars minimum.')
            .max(64, 'Password too long.')
            .matches(/^[A-Za-z0-9]+([A-Za-z0-9]*|[._-]?[A-Za-z0-9]+)*$/, {
              message: 'Should be almost one number and letter',
            }),
        }),
      ],
      validateOn: ['change'],
    },
    name: {
      init: '',
      rules: [
        createRule<string>({
          name: 'name',
          schema: yup
            .string()
            .required("You missed a spot! Don't forget to add your name.")
            .min(3, 'Should be 3 chars minimum.')
            .max(10, 'Name too long.')
            .matches(/^[A-Za-z0-9]+([A-Za-z0-9]*|[._-]?[A-Za-z0-9]+)*$/, {
              message: 'Please enter correct name',
            }),
        }),
      ],
      validateOn: ['change'],
    },
    age: {
      init: '',
      rules: [
        createRule<string>({
          name: 'age',
          schema: yup
            .number()
            .min(14, 'You too young.')
            .max(200, 'Please enter correct age.')
            .required("You missed a spot! Don't forget to add your age."),
        }),
      ],
      validateOn: ['change'],
    },
  },
});

sample({
  clock: [
    registerForm.fields.email.$isDirty,
    registerForm.fields.email.$isValid,
  ],
  source: registerForm.fields.email.$value,
  target: registerForm.fields.name.onChange,
  fn: (email) => {
    return email.split('@')[0];
  },
});
