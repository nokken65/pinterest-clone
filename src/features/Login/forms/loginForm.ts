import { createForm } from 'effector-forms';
import * as yup from 'yup';

import { createRule } from '@/shared/utils';

export const loginForm = createForm({
  fields: {
    email: {
      init: '',
      rules: [
        createRule<string>({
          name: 'email',
          schema: yup
            .string()
            .required("You missed a spot! Don't forget to add your email.")
            .matches(
              /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              { excludeEmptyString: true, message: 'Not a valid email.' },
            ),
        }),
      ],
      validateOn: ['change'],
    },
    password: {
      init: '',
      rules: [
        createRule<string>({
          name: 'password',
          schema: yup
            .string()
            .required("You missed a spot! Don't forget to add your password.")
            .min(8, 'Should be 8 chars minimum.')
            .max(64, 'Password too long.')
            .matches(/^[A-Za-z0-9]+([A-Za-z0-9]*|[._-]?[A-Za-z0-9]+)*$/, {
              message: 'Should be almost one number and letter',
            }),
        }),
      ],
      validateOn: ['change'],
    },
  },
});
