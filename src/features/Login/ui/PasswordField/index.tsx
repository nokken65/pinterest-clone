import { reflect } from '@effector/reflect';

import { Form } from '@/shared/components';

import { loginForm } from '../../forms';

export const PasswordField = reflect({
  view: Form.PasswordField,
  bind: {
    type: 'password',
    name: 'password',
    placeholder: 'Password',
    value: loginForm.fields.password.$value,
    onChange: loginForm.fields.password.onChange,
    isDirty: loginForm.fields.password.$isDirty,
    isValid: loginForm.fields.password.$isValid,
    errors: loginForm.fields.password.$errors,
  },
});
