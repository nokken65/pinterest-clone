import { reflect } from '@effector/reflect';

import { Form } from '@/shared/components';

import { loginForm } from '../../forms';

export const EmailField = reflect({
  view: Form.Field,
  bind: {
    type: 'email',
    name: 'email',
    placeholder: 'Email',
    value: loginForm.fields.email.$value,
    onChange: loginForm.fields.email.onChange,
    isDirty: loginForm.fields.email.$isDirty,
    isValid: loginForm.fields.email.$isValid,
    errors: loginForm.fields.email.$errors,
  },
});
