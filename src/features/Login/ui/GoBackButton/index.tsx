import { reflect } from '@effector/reflect';
import PrevIcon from '@icons/arrow.svg';
import { useNavigate } from 'react-router-dom';

import { Button } from '@/shared/components';

import { loginForm } from '../../forms';
import styles from './index.module.scss';

type GoBackButtonViewProps = {
  resetLoginForm: () => void;
};

const GoBackButtonView = ({ resetLoginForm }: GoBackButtonViewProps) => {
  const navigate = useNavigate();

  const goBack = () => {
    resetLoginForm();
    navigate('/');
  };

  return <Button className={styles.btn} icon={<PrevIcon />} onClick={goBack} />;
};

export const GoBackButton = reflect({
  view: GoBackButtonView,
  bind: {
    resetLoginForm: loginForm.reset,
  },
});
