import { PropsWithChildren } from 'react';

import { Form } from '@/shared/components';

type LoginFormProps = PropsWithChildren<{}>;

export const LoginForm = ({ children }: LoginFormProps) => {
  return <Form>{children}</Form>;
};
