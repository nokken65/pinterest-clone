import { createRoot } from 'react-dom/client';

import App from '@/app';
import { createWrapperAndAppendToBody } from '@/shared/utils';

let container = document.getElementById('app-root');
if (!container) {
  container = createWrapperAndAppendToBody('app-root');
}

const root = createRoot(container);
root.render(
  // @ts-ignore
  <App />,
);
