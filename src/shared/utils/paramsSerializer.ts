// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const paramsSerializer = (params: any) => {
  const qs = Object.keys(params)
    .map((key) => {
      if (key === 'filter' || key === 'sort') {
        return params[key].length !== 0
          ? `${key}=${JSON.stringify(params[key])}`
          : '';
      }

      return `${key}=${params[key]}`;
    })
    .filter((param) => param !== '')
    .join('&');

  return qs;
};
