interface Props {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  components: Array<React.JSXElementConstructor<React.PropsWithChildren<any>>>;
  children: React.ReactNode;
}

export const ComposeComp = ({ components = [], children }: Props) => {
  return (
    <>
      {components.reduceRight((acc, Comp) => {
        return <Comp>{acc}</Comp>;
      }, children)}
    </>
  );
};
