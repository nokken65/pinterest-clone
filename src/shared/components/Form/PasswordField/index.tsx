import HiddenIcon from '@icons/hidden.svg';
import UnhiddenIcon from '@icons/unhidden.svg';
import { useState } from 'react';

import { Field } from '../Field';
import { FieldProps } from '../types';
import styles from './index.module.scss';

export const PasswordField = ({ ...props }: FieldProps<string>) => {
  const [hidden, setHidden] = useState<boolean>(true);

  return (
    <Field {...props} type={hidden ? 'password' : 'text'}>
      {hidden ? (
        <HiddenIcon className={styles.icon} onClick={() => setHidden(false)} />
      ) : (
        <UnhiddenIcon className={styles.icon} onClick={() => setHidden(true)} />
      )}
    </Field>
  );
};
