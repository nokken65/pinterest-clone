import { memo } from 'react';

import { ValidationError, Value } from '../types';
import styles from './index.module.scss';

type ErrorsFeedbackProps<T> = { errors: ValidationError<T>[] };

export const ErrorsFeedback = memo(
  <T extends Value>({ errors }: ErrorsFeedbackProps<T>) => {
    return errors.length !== 0 ? (
      <div className={styles.wrapper}>
        {errors.map((error) => (
          <span
            className={styles['feedback-invalid__errors']}
            key={error.errorText}
          >
            {error.errorText ?? 'error'}
          </span>
        ))}
      </div>
    ) : null;
  },
);
