import clsx from 'clsx';
import { FormHTMLAttributes, PropsWithChildren } from 'react';

import { ErrorsFeedback } from './ErrorsFeedback';
import { Field } from './Field';
import styles from './index.module.scss';
import { PasswordField } from './PasswordField';

type FormProps = PropsWithChildren<FormHTMLAttributes<HTMLFormElement> & {}>;

const Form = ({ children, className, ...props }: FormProps) => {
  return (
    <form className={clsx(styles.wrapper, className)} {...props}>
      {children}
    </form>
  );
};

Form.Field = Field;
Form.PasswordField = PasswordField;
Form.ErrorsFeedback = ErrorsFeedback;

export { Form };
export * from './types';
