// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'feedback-invalid__errors': string;
  'feedback-invalid__input': string;
  'feedback-valid__input': string;
  'field': string;
  'input': string;
  'passwordHide': string;
}
export const cssExports: CssExports;
export default cssExports;
