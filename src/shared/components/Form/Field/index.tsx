import clsx from 'clsx';

import { ErrorsFeedback } from '../ErrorsFeedback';
import { FieldProps } from '../types';
import styles from './index.module.scss';

export const Field = <T extends FieldProps>({
  value,
  onChange,
  isDirty,
  isValid,
  errors,
  name,
  type = 'text',
  className,
  children,
  ...props
}: T) => {
  return (
    <>
      <div
        className={clsx(
          styles.field,
          !isValid && styles['feedback-invalid__input'],
          isDirty && isValid && styles['feedback-valid__input'],
          className,
        )}
        role='textbox'
      >
        <input
          className={styles.input}
          id={`${name}-id`}
          type={type}
          value={value}
          onChange={(e) => onChange(e.target.value)}
          {...props}
        />
        {children}
      </div>
      <ErrorsFeedback errors={errors} />
    </>
  );
};
