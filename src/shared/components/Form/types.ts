import { InputHTMLAttributes, PropsWithChildren } from 'react';

export type Value = string | number | readonly string[] | undefined;

export type ValidationError<T> = {
  rule: string;
  value: T;
  errorText?: string | undefined;
};

export type FieldProps<T = any> = Omit<
  InputHTMLAttributes<HTMLInputElement>,
  'onChange'
> &
  PropsWithChildren<{
    value: T;
    onChange: (value: T) => void;
    isDirty: boolean;
    isValid: boolean;
    errors: ValidationError<T>[];
  }>;
