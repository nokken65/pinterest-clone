import LogoIcon from '@icons/logo.svg';
import clsx from 'clsx';

import { Size } from '@/shared/types';

import styles from './index.module.scss';

type LogoProps = {
  size?: Size;
  color?: 'light' | 'dark' | 'red';
};

export const Logo = ({ size = '4xl', color = 'red' }: LogoProps) => {
  return <LogoIcon className={clsx(styles[`_${size}`], styles[color])} />;
};
