import clsx from 'clsx';
import { ButtonHTMLAttributes } from 'react';

import styles from './Button.module.scss';

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  icon?: JSX.Element;
  label?: string;
};

export const Button = ({ icon, label, className, ...props }: ButtonProps) => {
  return (
    <button className={clsx(styles.button, className)} type='button' {...props}>
      {icon}
      {label && <p>{label}</p>}
    </button>
  );
};
