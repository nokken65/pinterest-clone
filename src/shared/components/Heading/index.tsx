import clsx from 'clsx';
import { createElement } from 'react';

import { Size } from '@/shared/types';

import styles from './index.module.scss';

type HeadingProps = {
  title: string;
  type?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
  size?: Size;
  color?: 'light' | 'dark';
  uppercase?: boolean;
  className?: string;
};

export const Heading = ({
  title,
  type = 'h1',
  size = 'md',
  color = 'dark',
  uppercase = false,
  className,
}: HeadingProps) => {
  return createElement(
    type,
    {
      className: clsx(
        styles.heading,
        styles[`_${size}`],
        styles[color],
        uppercase && styles.uppercase,
        className,
      ),
    },
    title,
  );
};
