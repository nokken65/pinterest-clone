import CloseIcon from '@icons/close.svg';
import { clearAllBodyScrollLocks, disableBodyScroll } from 'body-scroll-lock';
import { PropsWithChildren, useCallback, useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';
import { useNavigate } from 'react-router-dom';

import { createWrapperAndAppendToBody } from '@/shared/utils';

import { Button } from '../Button';
import styles from './index.module.scss';

type ModalProps = PropsWithChildren<{}>;

export const Modal = ({ children }: ModalProps) => {
  let root = document.getElementById('modal-root');

  if (!root) {
    root = createWrapperAndAppendToBody('modal-root');
  }

  const navigate = useNavigate();
  const ref = useRef<HTMLDivElement>(root as HTMLDivElement);

  const onDismiss = useCallback(
    (e: any) => {
      e.stopPropagation();
      navigate(-1);
    },
    [navigate],
  );

  useEffect(() => {
    const close = (e: KeyboardEvent) => {
      if (e.code === 'Escape') {
        onDismiss(e);
      }
    };
    window.addEventListener('keydown', close);

    disableBodyScroll(ref.current, { reserveScrollBarGap: true });

    return () => {
      window.removeEventListener('keydown', close);
      clearAllBodyScrollLocks();
    };
  }, [onDismiss]);

  const ModalLayout = (
    <div
      aria-hidden='true'
      className={styles.bg}
      ref={ref}
      role='link'
      tabIndex={-1}
      onClick={onDismiss}
    >
      <div className={styles.wrapper}>
        <div className={styles.head}>
          <Button icon={<CloseIcon />} onClick={onDismiss} />
        </div>
        {children}
      </div>
    </div>
  );

  return createPortal(ModalLayout, root);
};
