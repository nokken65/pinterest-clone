import FbIcon from '@icons/fb.svg';
import GoogleIcon from '@icons/google.svg';
import clsx from 'clsx';
import { Link } from 'react-router-dom';

import { ROUTE_PATHS } from '@/shared/constants';

import { Button } from '../../Button';
import styles from './index.module.scss';

export const LoginButtons = () => {
  return (
    <>
      <Link to={ROUTE_PATHS.login.index}>
        <Button
          className={clsx(styles.btn, styles.btn__fb)}
          icon={<FbIcon className={styles.btn__icon} />}
          label='Continue with Facebook'
          tabIndex={-1}
        />
      </Link>
      <Link to={ROUTE_PATHS.login.index}>
        <Button
          className={clsx(styles.btn, styles.btn__g)}
          icon={<GoogleIcon className={styles.btn__icon} />}
          label='Continue with Google'
          tabIndex={-1}
        />
      </Link>
    </>
  );
};
