import clsx from 'clsx';
import { PropsWithChildren } from 'react';
import {
  Link as LinkRouter,
  LinkProps as LinkRouterProps,
  useLocation,
} from 'react-router-dom';

import styles from './index.module.scss';

type LinkProps = PropsWithChildren<LinkRouterProps>;

const Link = ({ children, className, ...props }: LinkProps) => {
  return (
    <LinkRouter className={clsx(styles.link, className)} {...props}>
      {children}
    </LinkRouter>
  );
};

const LinkModal = (props: LinkProps) => {
  const location = useLocation();

  return <Link state={{ backgroundLocation: location }} {...props} />;
};

Link.Modal = LinkModal;

export { Link };
