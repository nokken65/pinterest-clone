// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36

type Keys = {
  [key: string]: 1;
};

const keys: Keys = { ArrowLeft: 1, ArrowUp: 1, ArrowRight: 1, ArrowDown: 1 };

export const useScroll = () => {
  const preventDefault = (e: Event) => {
    e.preventDefault();
  };

  const preventDefaultForScrollKeys = (e: KeyboardEvent): false | undefined => {
    if (keys[e.code]) {
      preventDefault(e);

      return false;
    }

    return undefined;
  };

  // modern Chrome requires { passive: false } when adding event
  let supportsPassive = false;
  try {
    // @ts-ignore
    window.addEventListener(
      'test',
      null,
      Object.defineProperty({}, 'passive', {
        get() {
          supportsPassive = true;
        },
      }),
    );
  } catch (e) {
    console.error(e);
  }

  const wheelOpt = supportsPassive ? { passive: false } : false;
  const wheelEvent =
    'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

  // call this to Disable
  function disableScroll() {
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
  }

  // call this to Enable
  function enableScroll() {
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    // @ts-ignore
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
    // @ts-ignore
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
  }

  return [enableScroll, disableScroll];
};
