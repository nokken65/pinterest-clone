import 'reseter.css';
import './styles/index.scss';

import { Routing } from '@/pages';

import { withProviders } from './providers';

const PureApp = () => {
  return <Routing />;
};

// eslint-disable-next-line import/no-default-export, @typescript-eslint/no-explicit-any
export default withProviders()(PureApp) as React.ComponentType<any>;
