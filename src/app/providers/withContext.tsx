import { JSXElementConstructor, ReactNode } from 'react';

import { ComposeComp } from '@/shared/utils';

const providers: JSXElementConstructor<any>[] = [];

export const withContext = (component: () => ReactNode) => () =>
  <ComposeComp components={providers}>{component()}</ComposeComp>;
