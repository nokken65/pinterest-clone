import { composeFunc } from '@/shared/utils';

import { withContext } from './withContext';
import { withRouter } from './withRouter';

export const withProviders = () => composeFunc(withRouter, withContext);
