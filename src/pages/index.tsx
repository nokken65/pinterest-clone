/* eslint-disable react/no-array-index-key */
import { lazy, Suspense } from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';

import { ROUTE_PATHS } from '@/shared/constants';

import { loginRoutes } from './Login';
import { registrationRoutes } from './Registration';

const WelcomePage = lazy(() => import('./Welcome'));
const ComingSoon = lazy(() => import('./ComingSoon'));

const Routing = () => {
  const location = useLocation();
  const state = location.state as { backgroundLocation?: Location };

  return (
    <Suspense fallback={<span />}>
      <Routes location={state?.backgroundLocation || location}>
        <Route path={ROUTE_PATHS.index}>
          {/* Welcome */}
          <Route index element={<WelcomePage />} />
          {/* Registration */}
          <Route
            element={registrationRoutes.element}
            path={registrationRoutes.path}
          >
            {registrationRoutes.children &&
              registrationRoutes.children.map(({ index, path, element }, i) => (
                <Route element={element} index={index} key={i} path={path} />
              ))}
          </Route>
          {/* Login */}
          <Route element={loginRoutes.element} path={loginRoutes.path}>
            {loginRoutes.children &&
              loginRoutes.children.map(({ index, path, element }, i) => (
                <Route element={element} index={index} key={i} path={path} />
              ))}
          </Route>
        </Route>
      </Routes>
      {/* Modal */}
      {state?.backgroundLocation && (
        <Routes>
          <Route element={<ComingSoon />} path={ROUTE_PATHS.comingsoon} />
        </Routes>
      )}
    </Suspense>
  );
};

export { Routing };
