import { Heading, Modal } from '@/shared/components';

import styles from './index.module.scss';

const ComingSoon = () => {
  return (
    <Modal>
      <div className={styles.wrapper}>
        <Heading uppercase size='xl' title='Coming soon...' />
      </div>
    </Modal>
  );
};

export default ComingSoon;
