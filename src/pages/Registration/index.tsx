import { lazy, Suspense } from 'react';
import { Navigate, Outlet, RouteObject } from 'react-router-dom';

import { RegisterForm, registerModel } from '@/features/Register';
import { ROUTE_PATHS } from '@/shared/constants';
import { Footer, Header } from '@/widgets/Registration';

import styles from './index.module.scss';

const EmailStepPage = lazy(() => import('./Steps/EmailStep'));
const PasswordStepPage = lazy(() => import('./Steps/PasswordStep'));
const AgeAndNameStepPage = lazy(() => import('./Steps/AgeAndNameStep'));

const stepPages = [
  <EmailStepPage />,
  <PasswordStepPage />,
  <AgeAndNameStepPage />,
];

const registrationStepRoutes: RouteObject[] = registerModel.selectors.$steps
  .getState()
  .map((step, i) => ({
    path: step.path,
    element: stepPages[i],
  }));

const RegistrationLayout = () => {
  return (
    <div className={styles.wrapper}>
      <Header />
      <RegisterForm>
        <Suspense fallback={<span>Loading...</span>}>
          <Outlet />
        </Suspense>
      </RegisterForm>
      <Footer />
    </div>
  );
};

export const registrationRoutes: RouteObject = {
  path: ROUTE_PATHS.register.index,
  element: <RegistrationLayout />,
  children: [
    ...registrationStepRoutes,
    {
      index: true,
      element: <Navigate to={ROUTE_PATHS.register.step1} />,
    },
    {
      path: '*',
      element: <Navigate to={ROUTE_PATHS.register.step1} />,
    },
  ],
};
