import { EmailField, NextStepButton, registerForm } from '@/features/Register';
import { Heading, Socials } from '@/shared/components';

import styles from './index.module.scss';

const EmailStep = () => {
  return (
    <>
      <Heading
        className={styles.heading}
        size='2xl'
        title="What's your email?"
      />
      <EmailField />
      <NextStepButton
        isDirty={registerForm.fields.email.$isDirty}
        isValid={registerForm.fields.email.$isValid}
      />
      <Socials.LoginButtons />
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default EmailStep;
