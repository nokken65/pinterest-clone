import { NextStepButton, registerForm } from '@/features/Register';
import { PasswordField } from '@/features/Register/ui/PasswordField';
import { Heading } from '@/shared/components';

import styles from './index.module.scss';

const PasswordStep = () => {
  return (
    <>
      <Heading
        className={styles.heading}
        size='2xl'
        title='Create a password'
      />
      <PasswordField />
      <NextStepButton
        isDirty={registerForm.fields.password.$isDirty}
        isValid={registerForm.fields.password.$isValid}
      />
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default PasswordStep;
