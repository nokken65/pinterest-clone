import { AgeField, NameField } from '@/features/Register';
import { Heading } from '@/shared/components';

import styles from './index.module.scss';

const AgeAndNameStep = () => {
  return (
    <>
      <div className={styles.head}>
        <Heading size='2xl' title='How old are you?,' />
        <NameField />
      </div>
      <p className={styles.info}>
        This helps personalize ideas for your home feed. We won&#39;t show this
        on your profile.
      </p>
      <AgeField />
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default AgeAndNameStep;
