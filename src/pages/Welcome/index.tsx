import { Cards, Footer, WelcomeLogin } from '@/widgets/Welcome';

import styles from './index.module.scss';

const Welcome = () => {
  return (
    <div className={styles.wrapper}>
      <WelcomeLogin />
      <Cards />
      <WelcomeLogin
        title="Sign up to explore the world's best ideas"
        width='24rem'
      />
      <Footer />
    </div>
  );
};

export default Welcome;
