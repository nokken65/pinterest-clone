import { lazy, Suspense } from 'react';
import { Navigate, Outlet, RouteObject } from 'react-router-dom';

import { LoginForm } from '@/features/Login';
import { ROUTE_PATHS } from '@/shared/constants';
import { Footer, Header } from '@/widgets/Login';

import styles from './index.module.scss';

const LoginStep = lazy(() => import('./LoginStep'));

const LoginLayout = () => {
  return (
    <div className={styles.wrapper}>
      <Header />
      <LoginForm>
        <Suspense fallback={<span>Loading...</span>}>
          <Outlet />
        </Suspense>
      </LoginForm>
      <Footer />
    </div>
  );
};

export const loginRoutes: RouteObject = {
  path: ROUTE_PATHS.login.index,
  element: <LoginLayout />,
  children: [
    {
      index: true,
      element: <LoginStep />,
    },
    {
      path: '*',
      element: <Navigate to={ROUTE_PATHS.login.index} />,
    },
  ],
};
