import { EmailField, PasswordField } from '@/features/Login';
import { Heading, Link, Socials } from '@/shared/components';
import { ROUTE_PATHS } from '@/shared/constants';

import styles from './index.module.scss';

const LoginStep = () => {
  return (
    <>
      <Socials.LoginButtons />
      <Heading
        uppercase
        className={styles.heading}
        size='md'
        title='Or'
        type='h3'
      />
      <EmailField />
      <PasswordField />
      <Link.Modal className={styles.forgot} to={ROUTE_PATHS.comingsoon}>
        Forgot your password?
      </Link.Modal>
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default LoginStep;
