import { Link } from '@/shared/components';
import { ROUTE_PATHS } from '@/shared/constants';

import styles from './index.module.scss';

export const Footer = () => {
  return (
    <footer className={styles.wrapper}>
      <Link.Modal to={ROUTE_PATHS.comingsoon}>Users</Link.Modal>
      <Link.Modal to={ROUTE_PATHS.comingsoon}>Collections</Link.Modal>
      <Link.Modal to={ROUTE_PATHS.comingsoon}>Today</Link.Modal>
      <Link.Modal to={ROUTE_PATHS.comingsoon}>Explore</Link.Modal>
    </footer>
  );
};
