import clsx from 'clsx';

import { Button, Heading, Link, Logo, Socials } from '@/shared/components';
import { ROUTE_PATHS } from '@/shared/constants';

import styles from './index.module.scss';

type WelcomeLoginProps = {
  title?: string;
  width?: string;
};

export const WelcomeLogin = ({
  title = 'Welcome to Pinterest',
  width = '18rem',
}: WelcomeLoginProps) => {
  return (
    <div className={styles.wrapper} style={{ width }}>
      <Logo color='light' />
      <Heading color='light' size='3xl' title={title} type='h1' />
      <div className={styles['btns-container']}>
        <Link to={ROUTE_PATHS.register.index}>
          <Button
            className={clsx(styles.btn, styles.btn__email)}
            label='Continue with email'
            role='link'
            tabIndex={-1}
          />
        </Link>
        <Socials.LoginButtons />
      </div>
      <div className={styles['links-container']}>
        <Link to={ROUTE_PATHS.login.index}>Already a member? Log in</Link>
        <p>
          Are you a business?{' '}
          <Link.Modal to={ROUTE_PATHS.comingsoon}>Get started here</Link.Modal>
        </p>
        <p className={styles.terms}>
          By continuing, you agree to Pinterest&#39;s{' '}
          <Link.Modal to={ROUTE_PATHS.comingsoon}>Terms of Service</Link.Modal>{' '}
          and acknowledge you&#39;ve read our{' '}
          <Link.Modal to={ROUTE_PATHS.comingsoon}>Privacy Policy</Link.Modal>
        </p>
      </div>
    </div>
  );
};
