import { memo } from 'react';

import { Link } from '@/shared/components';
import { ROUTE_PATHS } from '@/shared/constants';

import styles from './index.module.scss';

export const Footer = memo(() => {
  return (
    <footer className={styles.wrapper}>
      <Link to={ROUTE_PATHS.register.index}>No Account? Sign up</Link>
      <p>
        Are you a business?{' '}
        <Link.Modal to={ROUTE_PATHS.comingsoon}>Get started here</Link.Modal>
      </p>
      <p className={styles.terms}>
        By continuing, you agree to Pinterest&#39;s{' '}
        <Link.Modal to={ROUTE_PATHS.comingsoon}>Terms of Service</Link.Modal>{' '}
        and acknowledge you&#39;ve read our{' '}
        <Link.Modal to={ROUTE_PATHS.comingsoon}>Privacy Policy</Link.Modal>
      </p>
    </footer>
  );
});
