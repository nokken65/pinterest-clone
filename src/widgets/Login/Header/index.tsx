import { memo } from 'react';

import { GoBackButton } from '@/features/Login';

import styles from './index.module.scss';

export const Header = memo(() => {
  return (
    <header className={styles.wrapper}>
      <GoBackButton />
      <span className={styles.empty} />
    </header>
  );
});
