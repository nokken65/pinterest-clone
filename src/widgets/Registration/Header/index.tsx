import { memo } from 'react';

import { PrevStepButton, StepsIndicator } from '@/features/Register';

import styles from './index.module.scss';

export const Header = memo(() => {
  return (
    <header className={styles.wrapper}>
      <PrevStepButton />
      <StepsIndicator />
      <span className={styles.empty} />
    </header>
  );
});
