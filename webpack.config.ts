import dotenv from 'dotenv';
import { merge } from 'webpack-merge';

import { commonConfig } from './webpack/common/common.config';
import { devConfig } from './webpack/development/config';
import { prodConfig } from './webpack/production/config';

dotenv.config();

const IS_DEVELOPMENT =
  !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const config = IS_DEVELOPMENT
  ? merge([commonConfig, devConfig])
  : merge([commonConfig, prodConfig]);

// eslint-disable-next-line import/no-default-export
export default config;
